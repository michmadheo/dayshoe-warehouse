import {observable} from 'mobx';

export default inventoryStore = observable({
    cleaner100 : 0,
    cleaner250 : 0,
    foam : 0,
    microFiber : 0,
    pBrush : 0,
    pBubble : 0,
    pFurizu : 0,
    pKohi : 0,
    premiumKit : 0,
    sBrush : 0,
    slimKit : 0,
    starterKit : 0,
    total : 0
});