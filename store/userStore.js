import {observable} from 'mobx';

export default userStore = observable({
    fname: "",
    lname: "",
    phone: "",
    inv_access: null
});