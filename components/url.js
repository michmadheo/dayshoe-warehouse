//////DATABASE//////////
export const DB_URL = 'https://dayshoewarehousedb.firebaseio.com/';
export const DB_PROJECT_ID = 'dayshoewarehousedb';
export const LOGIN = "https://dayshoewarehousedb.firebaseio.com/users/loginData.json";
export const GET_USER_DATA = "https://dayshoewarehousedb.firebaseio.com/users/userData/";
export const GET_INVENTORY = "https://dayshoewarehousedb.firebaseio.com/users/userData/";
//////STOREPAGE/////////
export const dayshoeTokopedia = "https://www.tokopedia.com/dayshoe?source=universe&st=product";