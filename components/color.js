export const UTAMA  = '#ffffff';
export const BIRU_MUDA  = '#62c7cd';
export const BIRU_MUDA_2  = '#62c7cd';
export const PUTIH  = '#ffffff';
export const PUTIH_REDUP  = '#f7f7f7';
export const GARIS  = '#e7e7e7';
export const BACKGROUND  = '#ededed';
export const HITAM  = '#000000';
export const HITAM_REDUP  = '#1b4848';
export const ABU  = '#8d8d8d';

/////WARNA KATEGORI////
export const BIRU  = '#60bacd';
export const ORANYE  = '#ffae65';
export const MERAH  = '#ff6f58';
export const WIBU  = '#ff7979';
