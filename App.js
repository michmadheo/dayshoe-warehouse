import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {Welcome} from "./pages/welcome";
import {Main} from './pages/main';
import {Dashboard} from './pages/dashboard';
import {DeliverPage} from "./pages/deliverPage";
import {TransferPage} from "./pages/transferPage";
import {RestockPage} from "./pages/restockPage";
import {TokopediaPage} from "./pages/tokopediaPage";

const navigation = createStackNavigator({
    Welcome: {screen: Welcome},
    Main: {screen: Main},
    Dashboard: {screen: Dashboard},
    DeliverPage: {screen: DeliverPage},
    TransferPage: {screen: TransferPage},
    RestockPage: {screen: RestockPage},
    TokopediaPage: {screen: TokopediaPage}
});

const App = createAppContainer(navigation);

export default App;

