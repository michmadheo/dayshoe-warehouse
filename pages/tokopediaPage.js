import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import {WebView} from 'react-native-webview';
import userStore from '../store/userStore';
import inventoryStore from '../store/inventoryStore';
import * as COLORS from '../components/color';
import {Back} from "../assets/images";
import SwiperFlatList from 'react-native-swiper-flatlist';
import {dayshoeTokopedia} from "../components/url";

export class TokopediaPage extends Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount(){
    }

    render() {
        return (
            <View style={{ flex: 1}}>
                <WebView source={{uri:dayshoeTokopedia}}/>
                <View style={{height:60, width: '100%', justifyContent:'center', position:'absolute', top:0, backgroundColor:COLORS.PUTIH}}>
                    <View style={{flexDirection:'row', paddingLeft:10, alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.replace('Dashboard')}}>
                            <Image source={Back} style={{width:25, height:25}}/>
                        </TouchableOpacity>
                        <Text style={{fontSize:20, paddingLeft:20}}>Tokopedia</Text>
                    </View>
                </View>
            </View>
        );
    }
}