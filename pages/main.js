import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, AsyncStorage, ActivityIndicator, Image, KeyboardAvoidingView } from 'react-native';
import * as COLORS from '../components/color';
import userStore from '../store/userStore';
import {DB_URL, DB_PROJECT_ID, LOGIN, GET_USER_DATA} from '../components/url';
import {Shoes, Back, hidePass, viewPass, dayShoe1, dayShoeLogo, dayShoeLogoPlain} from "../assets/images";

var firebase = require("firebase");
var config = {
    databaseURL: DB_URL,
    projectId: DB_PROJECT_ID,
};
if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

export class Main extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputID: "",
            inputPass: "",
            login:[],
            loadingProgress:false,
            wrongPassword:false,
            hidePass:true,
        }
    }

    static navigationOptions = {
        header: null
    }

    render() {
        return (
                <KeyboardAvoidingView style={{flex: 1,justifyContent:'center',backgroundColor:COLORS.PUTIH}}>
                    <TouchableOpacity style={{marginHorizontal:15, marginTop:15}} onPress={()=>{this.props.navigation.replace('Welcome')}}>
                        <Image source={Back} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    {/*<View style={{backgroundColor:COLORS.PUTIH_REDUP, flex:0.2}}>*/}
                        {/*/!*<Text style={{color:COLORS.HITAM_REDUP, marginHorizontal:50, fontSize:24, fontWeight:'bold'}}>Welcome!</Text>*!/*/}
                        {/*/!*<Text style={{color:COLORS.HITAM_REDUP, marginHorizontal:50, fontSize:20}}>Let's get you going</Text>*!/*/}
                    {/*</View>*/}
                    {/*<View style={{flex: 0.8, backgroundColor:COLORS.PUTIH_REDUP, justifyContent:'center'}}>*/}
                    {/*</View>*/}
                    {/*{this.state.loadingProgress?*/}
                    {/*<ActivityIndicator hideWhenStopped={false} size="large" color={COLORS.BACKGROUND} />:*/}
                    {/*null}*/}
                    <View style={{flex: 1, backgroundColor:COLORS.PUTIH, marginHorizontal:30, justifyContent:'center'}}>
                        <View style={{alignItems:'center'}}>
                            <Image source={dayShoeLogoPlain} style={{width:200, height:200}}/>
                        </View>
                        <Text style={{color:COLORS.HITAM_REDUP, marginBottom:10, fontSize:24, fontWeight:'bold'}}>Sign in</Text>
                        <TextInput
                            secureTextEntry={false}
                            maxLength={12}
                            placeholder={"Username"}
                            style={{fontSize:20}}
                            onChangeText={(inputID) => this.setState({inputID})}
                            value={this.state.inputID}
                        />
                        <View style={{width:'100%', height:2, backgroundColor:COLORS.BACKGROUND}}>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <TextInput
                                secureTextEntry={this.state.hidePass}
                                placeholder={"Password"}
                                style={{fontSize:20, flex:1}}
                                onChangeText={(inputPass) => this.setState({inputPass})}
                                value={this.state.inputPass}
                            />
                            <TouchableOpacity onPress={()=>this.setState({
                                hidePass: !this.state.hidePass
                            })} style={{justifyContent:'center'}}>
                                <Image source={this.state.hidePass?hidePass:viewPass} style={{width:27, height:27}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:'100%', height:2, backgroundColor:COLORS.BACKGROUND, marginBottom:30}}>
                        </View>
                        <View style={{backgroundColor:COLORS.PUTIH, justifyContent:'center', alignItems:'center'}}>
                            {this.state.loadingProgress?
                                <ActivityIndicator style={{marginBottom:30}} hideWhenStopped={false} size="large" color={COLORS.BIRU_MUDA} />:
                                <TouchableOpacity style={{backgroundColor:COLORS.BIRU_MUDA,
                                    width:'70%',
                                    marginBottom:30,
                                    height:40,
                                    alignItems:'center',
                                    justifyContent:'center',
                                    borderRadius:10, shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 1,
                                    },
                                    shadowOpacity: 0.20,
                                    shadowRadius: 1.41,

                                    elevation: 2,}}
                                                  onPress={()=>{
                                                      if(this.state.inputID.length > 0 && this.state.inputPass.length > 0){
                                                          this.login()
                                                      }
                                                      else{
                                                      }
                                                  }}>
                                    <Text style={{color:COLORS.PUTIH, color:COLORS.PUTIH, fontSize:15}}>Sign In</Text>
                                </TouchableOpacity>}
                            {/*<Text style={{marginBottom:10}}>Initiated by Michael Amadheo</Text>*/}
                        </View>
                    </View>
                </KeyboardAvoidingView>
        );
    }

    writeUserData(){
        firebase.database().ref('content/books/2').push({
            title:"Weee",
            year:'2019'
        }).then((data)=>{
            //success callback
            console.log('data ' , data)
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
        })
    }

    updateUserData(){
        firebase.database().ref('users/').update({
            data:'Michael'
        }).then((data)=>{
            //success callback
            console.log('data ' , data)
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
        })
    }

    readUserData() {
        firebase.database().ref('content/').once('value', function (snapshot) {
            console.log(snapshot.val())
        });
    }

    login(){
        this.setState({
            loadingProgress:true
        })
        let request = LOGIN;
        let length = 0
        let searchParam = 0
        let searchStatus = false
        let wrongPass = false
        return fetch(request)
            .then((response) => response.json())
            .then((responseJson) => {
                length = responseJson.length
                for(let i=0; i<length; i++){
                    searchParam++
                    if(responseJson[i].id === this.state.inputID){
                        if(responseJson[i].password === this.state.inputPass){
                            searchStatus=true
                            this.setState({
                                loadingProgress:true
                            })
                            AsyncStorage.setItem('LoggedIn', 'true')
                            length=i
                            this.getUserData();
                        }
                        else{
                            this.setState({
                                loadingProgress:false
                            })
                            i=length
                            wrongPass=true
                            alert('Salah Password bossque')
                        }
                    }
                }
                if(searchStatus===false && wrongPass===false && responseJson[length-1].id !== this.state.inputID){
                    alert('Tidak Ditemukan')
                    this.setState({
                        loadingProgress:false
                    })
                }

            })
            .catch((error) =>{
                console.error(error);
            });
    }

    getUserData(){
        let request = GET_USER_DATA+this.state.inputID+".json"
        return fetch(request)
            .then((response) => response.json())
            .then((responseJson) => {
                AsyncStorage.setItem('fname', responseJson.fname);
                AsyncStorage.setItem('lname', responseJson.lname);
                AsyncStorage.setItem('inv_access', responseJson.inv_access);
                AsyncStorage.setItem('phone', responseJson.phone);
                userStore.fname = responseJson.fname
                userStore.lname = responseJson.lname
                userStore.inv_access = responseJson.inv_access
                userStore.phone = responseJson.phone
                this.props.navigation.replace('Dashboard');
            })
            .catch((error) =>{
                console.error(error);
            });

    }

    async loadUserData(){
        let LoginStatus = await AsyncStorage.getItem('LoggedIn');
        userStore.fname = await AsyncStorage.getItem('fname');
        userStore.lname = await AsyncStorage.getItem('lname');
        userStore.inv_access = await AsyncStorage.getItem('inv_access');
        userStore.phone = await AsyncStorage.getItem('phone');
        if(LoginStatus === "true"){
            this.props.navigation.replace('Store')
        }
    }

}