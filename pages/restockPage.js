import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import userStore from '../store/userStore';
import inventoryStore from '../store/inventoryStore';
import * as COLORS from '../components/color';
import {Back} from "../assets/images";
import SwiperFlatList from 'react-native-swiper-flatlist';

export class RestockPage extends Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount(){
    }

    render() {
        return (
            <View style={{ flex: 1}}>
                <View style={{height:50, width: '100%', justifyContent:'center'}}>
                    <View style={{flexDirection:'row', paddingLeft:10, alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.replace('Dashboard')}}>
                        <Image source={Back} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize:20, paddingLeft:20}}>Restock</Text>
                    </View>
                </View>
            </View>
        );
    }
}