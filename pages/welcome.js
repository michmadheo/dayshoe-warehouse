import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import userStore from '../store/userStore';
import * as COLORS from '../components/color';
import {WelcomeArt} from "../assets/images";
import SwiperFlatList from 'react-native-swiper-flatlist';

export class Welcome extends Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount(){
        this.loadUserData()
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent:'center'}}>
                {/*<TouchableOpacity onPress={()=>{this.logOut()}}>*/}
                {/*<Text>Logout</Text>*/}
                {/*</TouchableOpacity>*/}
                <View style={{flex:1, paddingHorizontal:10, paddingTop:10, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize:29, fontWeight:'bold', color:COLORS.HITAM_REDUP}}><Text style={{color:COLORS.HITAM_REDUP}}>Welcome to,</Text> <Text style={{color:COLORS.BIRU_MUDA}}>Dayshoe</Text>.</Text>
                    <Text style={{fontSize:17,color:COLORS.HITAM_REDUP}}>One stop app for Dayshoe warehouse</Text>
                </View>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginVertical:20}}>
                    <Image source={WelcomeArt} style={{width:300, height:300}}/>
                    <Text style={{fontSize:15,color:COLORS.HITAM_REDUP}}>Manage your inventory on-the-go with ease</Text>
                </View>
                {/*<SwiperFlatList*/}
                    {/*style={{}}*/}
                    {/*autoplay*/}
                    {/*autoplayDelay={2}*/}
                    {/*autoplayLoop*/}
                    {/*showPagination*/}
                {/*>*/}
                    {/*<View style={{justifyContent:'center', alignItems:'center', marginHorizontal:50}}>*/}
                        {/*<Image source={WelcomeArt} style={{width:300, height:300}}/>*/}
                        {/*<Text style={{fontSize:15,color:COLORS.HITAM_REDUP}}>Manage your inventory on-the-go with ease</Text>*/}
                    {/*</View>*/}
                {/*</SwiperFlatList>*/}
                <View style={{flex:1,paddingHorizontal:10, paddingTop:10, justifyContent:'center', alignItems:'center'}}>
                    <TouchableOpacity style={{backgroundColor:COLORS.BIRU_MUDA,
                        width:'80%',
                        marginBottom:30,
                        marginTop:30,
                        height:40,
                        alignItems:'center',
                        justifyContent:'center',
                        borderRadius:10, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,

                        elevation: 2,}}
                                      onPress={()=>{this.props.navigation.replace('Main')
                                      }}>
                        <Text style={{color:COLORS.PUTIH, color:COLORS.PUTIH, fontSize:15}}>Get Started</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    async loadUserData(){
        let LoginStatus = await AsyncStorage.getItem('LoggedIn');
        userStore.fname = await AsyncStorage.getItem('fname');
        userStore.lname = await AsyncStorage.getItem('lname');
        userStore.inv_access = await AsyncStorage.getItem('inv_access');
        userStore.phone = await AsyncStorage.getItem('phone');
        if(LoginStatus === "true"){
            this.props.navigation.replace('Dashboard')
        }
    }

    async logOut(){
        await AsyncStorage.removeItem('LoggedIn', '')
        await AsyncStorage.removeItem('fname', '')
        await AsyncStorage.removeItem('lname', '')
        await AsyncStorage.removeItem('phone', '')
        await AsyncStorage.removeItem('inv_access', '')
        this.props.navigation.replace('Main')

    }

    async loadUserInfo(){
        let fname = await AsyncStorage.getItem('fname')
        userStore.fname = fname
    }
}