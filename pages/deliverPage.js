import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image, TextInput, ScrollView, Modal, ActivityIndicator} from 'react-native';
import userStore from '../store/userStore';
import inventoryStore from '../store/inventoryStore';
import * as COLORS from '../components/color';
import {Back} from "../assets/images";
import {DB_PROJECT_ID, DB_URL, GET_INVENTORY} from "../components/url";

var firebase = require("firebase");
var config = {
    databaseURL: DB_URL,
    projectId: DB_PROJECT_ID,
};
if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

export class DeliverPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            item:5,
            /////ITEM/////
            cleaner100:inventoryStore.cleaner100,
            cleaner250:inventoryStore.cleaner250,
            foam : inventoryStore.foam,
            microFiber : inventoryStore.microFiber,
            pBrush : inventoryStore.pBrush,
            pBubble : inventoryStore.pBubble,
            pFurizu : inventoryStore.pFurizu,
            pKohi : inventoryStore.pKohi,
            premiumKit : inventoryStore.premiumKit,
            sBrush : inventoryStore.sBrush,
            slimKit : inventoryStore.slimKit,
            starterKit : inventoryStore.starterKit,
            total : inventoryStore.total,
            /////INPUT////
            inputStarterKit: 0,
            inputSlimKit: 0,
            inputPremiumKit: 0,
            inputCleaner100: 0,
            inputCleaner250: 0,
            inputFoam: 0,
            inputSBrush: 0,
            inputPBrush: 0,
            inputMicroFiber: 0,
            inputPBubble: 0,
            inputPFurizu: 0,
            inputPKohi: 0,
            ////MODAL//////
            loading: false,
            modalVisible: false,

        }
    }

    static navigationOptions = {
        header: null
    }

    componentDidMount(){
    }

    render() {
        return (
            <View style={{ flex: 1}}>
                <View style={{height:50, width: '100%', justifyContent:'center'}}>
                    <View style={{flexDirection:'row', paddingLeft:10, alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.replace('Dashboard')}}>
                        <Image source={Back} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize:20, paddingLeft:20}}>Deliver</Text>
                    </View>
                </View>
                <ScrollView style={{marginLeft:20, flex:1}}>
                    {this.state.starterKit > 0?
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text>Starter Kit</Text>
                        <TextInput
                            secureTextEntry={false}
                            keyboardType={"numeric"}
                            maxLength={12}
                            placeholder={"0"}
                            style={{fontSize:14}}
                            onChangeText={(inputStarterKit) => this.setState({inputStarterKit})}
                            value={this.state.inputStarterKit}
                        />
                        <Text>/{this.state.starterKit}</Text>
                    </View>:null}
                    {this.state.slimKit > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Slim Kit</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputSlimKit) => this.setState({inputSlimKit})}
                                value={this.state.inputSlimKit}
                            />
                            <Text>/{this.state.slimKit}</Text>
                        </View>:null}
                    {this.state.premiumKit > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Premium Kit</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputPremiumKit) => this.setState({inputPremiumKit})}
                                value={this.state.inputPremiumKit}
                            />
                            <Text>/{this.state.premiumKit}</Text>
                        </View>:null}
                    {this.state.cleaner100 > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Cleaner 100ml</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputCleaner100) => this.setState({inputCleaner100})}
                                value={this.state.inputCleaner100}
                            />
                            <Text>/{this.state.cleaner100}</Text>
                        </View>:null}
                    {this.state.cleaner250 > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Cleaner 250ml</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputCleaner250) => this.setState({inputCleaner250})}
                                value={this.state.inputCleaner250}
                            />
                            <Text>/{this.state.cleaner250}</Text>
                        </View>:null}
                    {this.state.foam > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Shoe Foam</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputFoam) => this.setState({inputFoam})}
                                value={this.state.inputFoam}
                            />
                            <Text>/{this.state.foam}</Text>
                        </View>:null}
                    {this.state.sBrush > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Standard Brush</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputSBrush) => this.setState({inputSBrush})}
                                value={this.state.inputSBrush}
                            />
                            <Text>/{this.state.sBrush}</Text>
                        </View>:null}
                    {this.state.pBrush > 0?
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text>Premium Brush</Text>
                        <TextInput
                            secureTextEntry={false}
                            keyboardType={"numeric"}
                            maxLength={12}
                            placeholder={"0"}
                            style={{fontSize:14}}
                            onChangeText={(inputPBrush) => this.setState({inputPBrush})}
                            value={this.state.inputPBrush}
                        />
                        <Text>/{this.state.pBrush}</Text>
                    </View>:null}
                    {this.state.microFiber > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Micro Fiber</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputMicroFiber) => this.setState({inputMicroFiber})}
                                value={this.state.inputMicroFiber}
                            />
                            <Text>/{this.state.microFiber}</Text>
                        </View>:null}
                    {this.state.pBubble > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Perfume Baburugamu</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputPBubble) => this.setState({inputPBubble})}
                                value={this.state.inputPBubble}
                            />
                            <Text>/{this.state.pBubble}</Text>
                        </View>:null}
                    {this.state.pFurizu > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Perfume Furizu</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputPFurizu) => this.setState({inputPFurizu})}
                                value={this.state.inputPFurizu}
                            />
                            <Text>/{this.state.pFurizu}</Text>
                        </View>:null}
                    {this.state.pKohi > 0?
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text>Perfume Kohi</Text>
                            <TextInput
                                secureTextEntry={false}
                                keyboardType={"numeric"}
                                maxLength={12}
                                placeholder={"0"}
                                style={{fontSize:14}}
                                onChangeText={(inputPKohi) => this.setState({inputPKohi})}
                                value={this.state.inputPKohi}
                            />
                            <Text>/{this.state.pKohi}</Text>
                        </View>:null}
                </ScrollView>
                <View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=>{this.checkInventory()}} style={{flex:1, height:50, backgroundColor:COLORS.BIRU_MUDA_2, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:COLORS.PUTIH, fontSize:18}}>Deliver</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{this.reset()}} style={{flex:1, height:50, backgroundColor:COLORS.BIRU_MUDA_2, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:COLORS.PUTIH, fontSize:18}}>Reset</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.state.modalVisible}
                       onRequestClose = {() => { console.log("Modal has been closed.") } }>

                    <View style = {{flex: 1,
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.2)', justifyContent:'center', alignItems:'center'}}>
                        {this.state.loading?
                            <ActivityIndicator style={{marginBottom:30}} hideWhenStopped={false} size="large" color={COLORS.BIRU_MUDA}/>:
                            <View style={{backgroundColor:COLORS.PUTIH, width:250, height:150, borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                                <Text style = {{
                                    color: COLORS.HITAM,
                                    marginTop: 10, alignSelf:'center', marginBottom:20
                                }}>Confirm item delivery?</Text>
                                <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{
                                        this.countItems()
                                        this.setState({
                                            loading:true
                                        })
                                    }}>
                                        <Text>Yes</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{flex:1, alignItems:'center'}}
                                                      onPress={()=>{
                                                          this.setState({modalVisible:false})}}>
                                        <Text>No</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                    </View>
                </Modal>
            </View>
        );
    }

    checkInventory(){
        if(this.state.inputStarterKit > this.state.starterKit){
            alert("Your Starter Kit stock is lower than the amount")
        }
        else if(this.state.inputSlimKit > this.state.slimKit){
            alert("Your Slim Kit stock is lower than the amount")
        }
        else if(this.state.inputPremiumKit > this.state.premiumKit){
            alert("Your Premium Kit stock is lower than the amount")
        }
        else if(this.state.inputCleaner100 > this.state.cleaner100){
            alert("Your 100ml Cleaner stock is lower than the amount")
        }
        else if(this.state.inputCleaner250 > this.state.cleaner250){
            alert("Your 250ml Cleaner stock is lower than the amount")
        }
        else if(this.state.inputFoam > this.state.foam){
            alert("Your Shoe Foam stock is lower than the amount")
        }
        else if(this.state.inputSBrush > this.state.sBrush){
            alert("Your Standard Brush stock is lower than the amount")
        }
        else if(this.state.inputPBrush > this.state.pBrush){
            alert("Your Premium Brush stock is lower than the amount")
        }
        else if(this.state.inputMicroFiber > this.state.microFiber){
            alert("Your Micro Fiber stock is lower than the amount")
        }
        else if(this.state.inputPBubble > this.state.pBubble){
            alert("Your Baburugamu Perfume stock is lower than the amount")
        }
        else if(this.state.inputPFurizu > this.state.pFurizu){
            alert("Your Furizu Perfume stock is lower than the amount")
        }
        else if(this.state.inputPKohi > this.state.pKohi){
            alert("Your Kohi Perfume stock is lower than the amount")
        }
        else if(
            this.state.inputStarterKit < 1 &&
            this.state.inputSlimKit < 1 &&
            this.state.inputPremiumKit < 1 &&
            this.state.inputCleaner100 < 1 &&
            this.state.inputCleaner250 < 1 &&
            this.state.inputFoam < 1 &&
            this.state.inputSBrush < 1 &&
            this.state.inputPBrush < 1 &&
            this.state.inputMicroFiber < 1 &&
            this.state.inputPBubble < 1 &&
            this.state.inputPFurizu < 1 &&
            this.state.inputPKohi < 1
        ){
            alert('No Item selected')
        }
        else{
            // this.countItems()
            this.setState({
                modalVisible: true
            })
        }
    }

    reset(){
        this.setState({
            inputCleaner100:0,
            inputCleaner250:0,
            inputFoam :0,
            inputMicroFiber :0,
            inputPBrush :0,
            inputPBubble :0,
            inputPFurizu :0,
            inputPKohi :0,
            inputPremiumKit :0,
            inputSBrush :0,
            inputSlimKit :0,
            inputStarterKit :0,
        })
    }

    async countItems(){
        let request = GET_INVENTORY+userStore.fname+"/inventory.json"
        return fetch(request)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    cleaner100:responseJson.cleaner100 - this.state.inputCleaner100,
                    cleaner250:responseJson.cleaner250 - this.state.inputCleaner250,
                    foam : responseJson.foam - this.state.inputFoam,
                    microFiber : responseJson.microFiber - this.state.inputMicroFiber,
                    pBrush : responseJson.pBrush - this.state.inputPBrush,
                    pBubble : responseJson.pBubble - this.state.inputPBubble,
                    pFurizu : responseJson.pFurizu - this.state.inputPFurizu,
                    pKohi : responseJson.pKohi - this.state.inputPKohi,
                    premiumKit : responseJson.premiumKit - this.state.inputPremiumKit,
                    sBrush : responseJson.sBrush - this.state.inputSBrush,
                    slimKit : responseJson.slimKit - this.state.inputSlimKit,
                    starterKit : responseJson.starterKit - this.state.inputStarterKit,
                    // total : responseJson.total - total,
                }, ()=>this.updateUserData())
            })
            .catch((error) =>{
                console.error(error);
            });
    }

    updateUserData(){
        firebase.database().ref('users/userData/'+userStore.fname+'/inventory').update({
            cleaner100: this.state.cleaner100,
            cleaner250:this.state.cleaner250,
            foam : this.state.foam,
            microFiber : this.state.microFiber,
            pBrush : this.state.pBrush,
            pBubble : this.state.pBubble,
            pFurizu : this.state.pFurizu,
            pKohi : this.state.pKohi,
            premiumKit : this.state.premiumKit,
            sBrush : this.state.sBrush,
            slimKit : this.state.slimKit,
            starterKit : this.state.starterKit,
        }).then((data)=>{
            //success callback
            this.props.navigation.replace("Dashboard")
            this.setState({
                modalVisible:false
            })
            alert("Items successfully Delivered")
            console.log('data ' , data)
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
        })
    }
}