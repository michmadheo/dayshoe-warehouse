import React, { Component } from 'react';
import { Text, View, TouchableOpacity, AsyncStorage, Image, ScrollView, Clipboard, ActivityIndicator, FlatList, Modal, TouchableHighlight, Linking, BackHandler} from 'react-native';
import userStore from '../store/userStore';
import * as COLORS from '../components/color';
import {home, homeSelected, profile, profileSelected, inventory, inventorySelected, history, historySelected, emptyInventory, deliver, transfer, user,
restock} from '../assets/images'
import inventoryStore from "../store/inventoryStore";
import {GET_INVENTORY, dayshoeTokopedia} from '../components/url';
import {observer} from 'mobx-react';
import {RestockPage} from '../pages/restockPage';

export class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            date: 0,
            menu:1,
            inventory:false,
            dayshoeItem:[],
            refreshProduct:false,
            total:undefined,
            modalVisible:false,
        }
    }
    static navigationOptions = {
        header: null
    }

    componentDidMount(){
        this.getInventory()
        BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp()
        });
        // this.getDate()
        // this.loadUserInfo()
    }


    async getInventory(){
        let request = GET_INVENTORY+userStore.fname+"/inventory.json"
        return fetch(request)
            .then((response) => response.json())
            .then((responseJson) => {
                inventoryStore.cleaner100 = responseJson.cleaner100
                inventoryStore.cleaner250 = responseJson.cleaner250
                inventoryStore.foam = responseJson.foam
                inventoryStore.microFiber = responseJson.microFiber
                inventoryStore.pBrush = responseJson.pBrush
                inventoryStore.sBrush = responseJson.sBrush
                inventoryStore.pBubble = responseJson.pBubble
                inventoryStore.pKohi = responseJson.pKohi
                inventoryStore.pFurizu = responseJson.pFurizu
                inventoryStore.slimKit = responseJson.slimKit
                inventoryStore.starterKit = responseJson.starterKit
                inventoryStore.premiumKit = responseJson.premiumKit
                inventoryStore.total = responseJson.total
                this.state.dayshoeItem.push({name:"Slim Kit", amount:responseJson.slimKit, category:'Kit'},
                    {name:"Starter Kit", amount:responseJson.starterKit, category:'Kit'},
                    {name:"Premium Kit", amount:responseJson.premiumKit, category:'Kit'},
                    {name:"Cleaner 100ml", amount:responseJson.cleaner100, category:'Cleaner'},
                    {name:"Cleaner 250ml", amount:responseJson.cleaner250, category:'Cleaner'},
                    {name:"Shoe Foam", amount:responseJson.foam, category:'Cleaner'},
                    {name:"Standard Brush", amount:responseJson.sBrush, category:'Brush'},
                    {name:"Premium Brush", amount:responseJson.pBrush, category:'Brush'},
                    {name:"Micro Fiber", amount:responseJson.microFiber, category:'Brush'},
                    {name:"Perfume Baburugamu", amount:responseJson.pBubble, category:'Perfume'},
                    {name:"Perfume Furizu", amount:responseJson.pFurizu, category:'Perfume'},
                    {name:"Perfume Kohi", amount:responseJson.pKohi, category:'Perfume'})
                this.setState({
                    inventory:true,
                })
            })
            .catch((error) =>{
                console.error(error);
            });
    }

    refreshInventory(){
        this.setState({
            dayshoeItem: [],
            inventory: false,
            loaded: false,
            total: undefined,
        })
        inventoryStore.cleaner100 = 0
        inventoryStore.cleaner250 = 0
        inventoryStore.foam = 0
        inventoryStore.microFiber = 0
        inventoryStore.pBrush = 0
        inventoryStore.sBrush = 0
        inventoryStore.pBubble = 0
        inventoryStore.pKohi = 0
        inventoryStore.pFurizu = 0
        inventoryStore.slimKit = 0
        inventoryStore.starterKit = 0
        inventoryStore.premiumKit = 0
        inventoryStore.total = 0
        this.getInventory()
    }

    async getDate(){
        let date = new Date();
        this.setState({
            date: date.getDay()
        })
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor:COLORS.PUTIH_REDUP}}>
                {this.viewControl()}
                <View style={{backgroundColor:COLORS.PUTIH, height:50, width:'100%', justifyContent:'center',shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 4,
                    },
                    shadowOpacity: 0.32,
                    shadowRadius: 5.46,

                    elevation: 9,}}>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{this.setState({menu:1})}}>
                            <Image source={this.state.menu===1?homeSelected:home} style={{height:25, width:25}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{this.setState({menu:2})}}>
                            <Image source={this.state.menu===2?inventorySelected:inventory} style={{height:25, width:25}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{this.setState({menu:3})}}>
                            <Image source={this.state.menu===3?historySelected:history} style={{height:25, width:25}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{this.setState({menu:4})}}>
                            <Image source={this.state.menu===4?profileSelected:profile} style={{height:25, width:25}}/>
                        </TouchableOpacity>
                    </View>
                </View>
                {/*<Modal*/}
                    {/*animationType="slide"*/}
                    {/*transparent={false}*/}
                    {/*visible={this.state.modalVisible}*/}
                    {/*onRequestClose={() => {*/}
                        {/*alert('Modal has been closed.');*/}
                    {/*}}>*/}
                    {/*<View>*/}
                        {/*<View style={{backgroundColor: 'rgba(0,0,0,0.5)', alignItems:'center', justifyContent:'center'}}>*/}
                            {/*<View>*/}
                                {/*<Text>Hello World!</Text>*/}
                                {/*<TouchableOpacity onPress={()=>{this.setState({modalVisible:false})}}>*/}
                                    {/*<Text>CLOSE</Text>*/}
                                {/*</TouchableOpacity>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</Modal>*/}
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.state.modalVisible}
                       onRequestClose = {() => { console.log("Modal has been closed.") } }>

                    <View style = {{flex: 1,
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.2)', justifyContent:'center', alignItems:'center'}}>
                        <View style={{backgroundColor:COLORS.PUTIH, width:250, height:150, borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                            <Text style = {{
                                color: COLORS.HITAM,
                                marginTop: 10, alignSelf:'center', marginBottom:20
                            }}>Logout from application?</Text>
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{
                                    this.logOut()
                                    this.setState({modalVisible:false})}}>
                                    <Text>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>{this.setState({modalVisible:false})}}>
                                    <Text>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

    viewControl(){
        switch (this.state.menu){
            case 1:
                return(
                    this.homeView()
                )
            case 2:
                return(
                    this.teamView()
                )
            case 3:
                return(
                    this.historyView()
                )
            case 4:
                return(
                    this.profileView()
                )
        }
    }

    homeView(){
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor:COLORS.BIRU_MUDA_2}}>
                    <View style={{marginHorizontal:20, marginTop:10}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text style={{flex:1, fontSize:20, color:COLORS.PUTIH}}><Text style={{fontWeight:'bold'}}>Hello</Text>, <Text style={{color:COLORS.PUTIH}}>{userStore.fname}</Text></Text>
                            {/*<TouchableOpacity onPress={()=>{this.logOut()}}>*/}
                                {/*<Image source={user} style={{width:20, height:20}}/>*/}
                            {/*</TouchableOpacity>*/}
                        </View>
                        <Text style={{color:COLORS.PUTIH}}>Your Inventory</Text>
                    </View>
                </View>
                <View>
                    <View style={{backgroundColor:COLORS.BIRU_MUDA_2, height:40, width:'100%', borderBottomStartRadius:50, borderBottomEndRadius:50}}>
                    </View>
                    <View style={{backgroundColor:COLORS.PUTIH_REDUP, height:50, width:'100%'}}>
                    </View>
                    <View style={{backgroundColor:COLORS.PUTIH,
                        height:70,
                        width:'90%',
                        position:'absolute',
                        top:10,
                        alignSelf:'center',
                        borderRadius:10,
                        flexDirection:'row',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,

                        elevation: 2,}}>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontWeight:'bold', fontSize:20, color:COLORS.HITAM_REDUP}}>{this.totalKits()}</Text>
                            <Text style={{fontSize:12, color:COLORS.ABU}}>Cleaner kits</Text>
                        </View>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontWeight:'bold', fontSize:20, color:COLORS.HITAM_REDUP}}>{this.totalSingle()}</Text>
                            <Text style={{fontSize:12, color:COLORS.ABU}}>Single Items</Text>
                        </View>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontWeight:'bold', fontSize:20, color:COLORS.HITAM_REDUP}}>{this.totalAll()}</Text>
                            <Text style={{fontSize:12, color:COLORS.ABU}}>Total</Text>
                        </View>
                    </View>
                </View>
                <View style={{marginHorizontal:40, marginBottom:10}}>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity
                            onPress={()=>{
                                this.props.navigation.navigate("DeliverPage")
                                BackHandler.removeEventListener('hardwareBackPress');
                            }}
                            style={{
                            flex:1,
                            alignItems:'center',
                            justifyContent:'center',
                            backgroundColor:COLORS.PUTIH,
                            height:70,
                            width:70,
                            marginHorizontal:5,
                            borderRadius:10,
                            borderWidth:1,
                            borderColor:COLORS.BIRU_MUDA_2,
                        }}>
                            <Image source={deliver} style={{height:30, width:30}}/>
                            <Text style={{color:COLORS.ABU}}>Deliver</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>{this.props.navigation.navigate("TransferPage")}}
                            style={{
                            flex:1,
                            alignItems:'center',
                            justifyContent:'center',
                            backgroundColor:COLORS.PUTIH,
                            marginHorizontal:5,
                            borderRadius:10,
                            borderWidth:1,
                            borderColor:COLORS.BIRU_MUDA_2
                        }}>
                            <Image source={transfer} style={{height:30, width:30}}/>
                            <Text style={{color:COLORS.ABU}}>Transfer</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{this.restockFilter()}}
                                          style={{
                                              flex:1,
                                              alignItems:'center',
                                              justifyContent:'center',
                                              backgroundColor:COLORS.PUTIH,
                                              marginHorizontal:5,
                                              borderRadius:10,
                                              borderWidth:1,
                                              borderColor:COLORS.BIRU_MUDA_2}}>
                            <Image source={restock} style={{height:30, width:30}}/>
                            <Text style={{color:COLORS.ABU}}>Restock</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.totalAll()===0?
                    <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
                        <Image source={emptyInventory} style={{height:100, width:100}}/>
                        <Text style={{marginTop:20,color:COLORS.ABU, fontSize:15}}>Oops, Looks like your inventory is empty</Text>
                        <TouchableOpacity onPress={()=>{this.refreshInventory()}}>
                            <Text style={{color:COLORS.ABU, fontSize:15, textDecorationLine:'underline'}}>Retry</Text>
                        </TouchableOpacity>
                    </View>:null}
                {this.state.inventory?
                    <FlatList
                        data={this.state.dayshoeItem}
                        refreshing={this.state.refreshProduct}
                        onRefresh={()=>{
                            this.refreshInventory()
                        }}
                        renderItem={({item,index}) =>
                        {
                            if(item.amount>0){
                            return(
                                <View key={index.toString()} style={{marginHorizontal:20, flexDirection:'row'}}>
                                    <View style={{flex:1, backgroundColor:this.categoryColor(item.category), height:80, marginVertical:5,borderRadius:10, borderTopStartRadius:10, borderBottomStartRadius:10}}>
                                        <Text style={{color:COLORS.PUTIH, marginLeft:10, marginTop:3, fontSize:15, fontWeight:'bold'}}>{item.name}</Text>
                                        <Text style={{color:COLORS.PUTIH, marginLeft:10, marginTop:3, fontSize:25, fontWeight:'bold'}}>{item.amount}</Text>
                                    </View>
                                    {/*<View style={{flex:0.4, backgroundColor:COLORS.PUTIH, height:80, marginVertical:5, borderRadius:10, borderTopEndRadius:10, borderBottomEndRadius:10}}>*/}
                                    {/*</View>*/}
                                </View>
                            )}
                            else{return(null)}
                        }
                        }
                        keyExtractor={({id}, index) => id}
                    />
                    // <ScrollView style={{marginHorizontal:20}} showsVerticalScrollIndicator={false}>
                    //     {
                    //         this.state.dayshoeKit.map((data,index)=>{
                    //             if(data.amount > 0){
                    //                 return(
                    //                     <View key={index.toString()}>
                    //                         <View style={{backgroundColor:COLORS.PUTIH, height:80, marginVertical:5, }}>
                    //                             <Text>{data.name}</Text>
                    //                             <Text>{data.amount}</Text>
                    //                         </View>
                    //                     </View>
                    //                 )
                    //             }
                    //             else{
                    //                 return(null)
                    //             }
                    //         })
                    //     }
                    // </ScrollView>
                    :
                    <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
                        <ActivityIndicator style={{marginBottom:30}} hideWhenStopped={false} size="large" color={COLORS.BIRU_MUDA}/>
                    </View>}

            </View>
        )
    }

    restockFilter(){
        if(userStore.inv_access==="yes"){
            this.props.navigation.navigate("RestockPage")
        }
        else{
            alert("You are not allowed to re-stock your inventory because you are not a warehouse master.")
        }
    }

    categoryColor(item){
        if (item ==="Kit"){
            return(
                COLORS.BIRU
            )
        }
        else if (item === "Cleaner"){
            return(
                COLORS.MERAH
            )
        }
        else if (item === "Brush"){
            return(
                COLORS.ORANYE
            )
        }
        else if (item === "Perfume"){
            return(
                COLORS.WIBU
            )
        }
    }

    teamView(){
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor:COLORS.BIRU_MUDA_2, height:50, width:'100%'}}>
                </View>
            </View>
        )
    }

    historyView(){
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor:COLORS.BIRU_MUDA_2, height:50, width:'100%'}}>
                </View>
            </View>
        )
    }

    profileView(){
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor:COLORS.BIRU_MUDA_2, height:40, width:'100%',justifyContent:'center'}}>
                    <Text style={{marginLeft:20, fontSize:18, color:COLORS.PUTIH}}>Profile</Text>
                </View>
                <View style={{flex:1, paddingTop:40, alignItems:'center', backgroundColor:COLORS.PUTIH}}>
                    <View style={{backgroundColor:COLORS.ORANYE, borderRadius:100, height:80, width:80, justifyContent:'center', alignItems:'center'}}>
                        <Text style={{color:COLORS.PUTIH,fontSize:40}}>{userStore.fname[0]}{userStore.lname[0]}</Text>
                    </View>
                    <Text style={{marginTop:10, fontSize:20, color:COLORS.HITAM_REDUP}}>{userStore.fname} {userStore.lname}</Text>
                    {/*<Text style={{marginTop:5, fontSize:17}}>{userStore.phone}</Text>*/}
                    <Text style={{marginTop:5, fontSize:17, marginBottom:10, color:COLORS.HITAM_REDUP}}>{userStore.inv_access==="yes"?"Master Warehouse":"Standard Warehouse"}</Text>
                </View>
                <View style={{flex:1, backgroundColor:COLORS.PUTIH, paddingHorizontal:20, }}>
                    {/*<TouchableOpacity onPress={()=>{alert("Coming Soon")}} style={{paddingBottom:10}}>*/}
                        {/*<Text style={{fontSize:17}}>Edit Profile</Text>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate("TokopediaPage")}} style={{paddingBottom:10}}>
                        <Text style={{marginTop:10, fontSize:17, color:COLORS.HITAM_REDUP}}>Visit store</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity onPress={()=>{this.logOut()}} style={{paddingBottom:10}}>*/}
                        {/*<Text style={{marginTop:10, fontSize:17, color:COLORS.MERAH}}>Log out</Text>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity onPress={()=>{this.setState({modalVisible:true})}} style={{paddingBottom:10}}>
                        <Text style={{marginTop:10, fontSize:17, color:COLORS.MERAH}}>Log out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    totalKits(){
        let total = inventoryStore.premiumKit + inventoryStore.starterKit + inventoryStore.slimKit
        return total

    }

    totalSingle(){
        let total = inventoryStore.cleaner250
            + inventoryStore.cleaner100
            + inventoryStore.foam
            + inventoryStore.microFiber
            + inventoryStore.pBubble
            + inventoryStore.pFurizu
            + inventoryStore.pKohi
            + inventoryStore.pBrush
            + inventoryStore.sBrush
        return total
    }

    totalAll(){
        let total = inventoryStore.cleaner250
            + inventoryStore.cleaner100
            + inventoryStore.foam
            + inventoryStore.microFiber
            + inventoryStore.pBubble
            + inventoryStore.pFurizu
            + inventoryStore.pKohi
            + inventoryStore.pBrush
            + inventoryStore.sBrush
            + inventoryStore.premiumKit
            + inventoryStore.starterKit
            + inventoryStore.slimKit
        return total
    }

    async logOut(){
        await AsyncStorage.removeItem('LoggedIn', '')
        await AsyncStorage.removeItem('fname', '')
        await AsyncStorage.removeItem('lname', '')
        await AsyncStorage.removeItem('phone', '')
        await AsyncStorage.removeItem('inv_access', '')
        this.props.navigation.replace('Main')
        inventoryStore.cleaner100 = 0
        inventoryStore.cleaner250 = 0
        inventoryStore.foam = 0
        inventoryStore.microFiber = 0
        inventoryStore.pBrush = 0
        inventoryStore.sBrush = 0
        inventoryStore.pBubble = 0
        inventoryStore.pKohi = 0
        inventoryStore.pFurizu = 0
        inventoryStore.slimKit = 0
        inventoryStore.starterKit = 0
        inventoryStore.premiumKit = 0
        inventoryStore.total = 0

    }
}